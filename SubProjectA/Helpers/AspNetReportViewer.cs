﻿using System;
using System.ComponentModel;
using Wisej.Web;
using Microsoft.Reporting.WebForms;
using Wisej.Web.Ext.AspNetControl;
using System.Data;
using System.Web;
using System.IO;

namespace SubProjectA
{
    public class AspNetReportViewer : AspNetWrapper<ReportViewer>
    {
        /// <summary>
        /// Returns or sets the URL of the Reporting Services server.
        /// </summary>
        /// 

        public string ServerUrl
        {
            get { return this._serverUrl; }

            set
            {
                value = value ?? "";

                if (this._serverUrl != value)
                {
                    this._serverUrl = value;
                    Update();
                }
            }
        }
        private string _serverUrl = "";

        /// <summary>
        /// Returns or sets the path to the report on the server.
        /// </summary>
        [DefaultValue("")]
        public string ReportPath
        {
            get { return this._reportPath; }

            set
            {
                value = value ?? "";

                if (this._reportPath != value)
                {
                    this._reportPath = value;
                    Update();
                }
            }
        }
        private string _reportPath = "";

        /// <summary>
        /// Returns or sets the authentication user name.
        /// </summary>
        [DefaultValue(null)]
        public string UserName
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the authentication password.
        /// </summary>
        [DefaultValue(null)]
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the authentication domain name.
        /// </summary>
        [DefaultValue(null)]
        public string DomainName
        {
            get;
            set;
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            //this.Text = "Loading report";

            //var viewer = this.WrappedControl;
            //viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            //viewer.LocalReport.ReportPath = "Report1.rdlc";

            //this.Text = "Report loaded";
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.Text = "Loading report";

            var viewer = this.WrappedControl;
            viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            viewer.LocalReport.ReportPath = "Report1.rdlc";

            this.Text = "Report loaded";
        }

        void viewer_BookmarkNavigation(object sender, BookmarkNavigationEventArgs e)
        {
            // this call is coming from the aspne handler. if the
            // event handler in the wisej application updates a control, we need
            // either call Application.Update, or run the handler in an Update block to
            // push any update back to the client after the block is executed.
            Application.Update(this, () =>
            {
                BookmarkNavigation(sender, e);
            });
        }

        void viewer_PageNavigation(object sender, PageNavigationEventArgs e)
        {
            Application.Update(this, () =>
            {
                PageNavigation(sender, e);
            });
        }

        void viewer_Toggle(object sender, CancelEventArgs e)
        {
            Application.Update(this, () =>
            {
                Toggle(sender, e);
            });
        }

        /// <summary>
        /// Toggle event.
        /// </summary>
        public event CancelEventHandler Toggle;

        /// <summary>
        /// PageNavigation event.
        /// </summary>
        public event PageNavigationEventHandler PageNavigation;

        /// <summary>
        /// BookmarkNavigation event.
        /// </summary>
        public event BookmarkNavigationEventHandler BookmarkNavigation;
    }
}