﻿
using System;
using Wisej.Web;

namespace SubProjectA
{
    public partial class Window1 : Form
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void Window1_Load(object sender, EventArgs e)
        {
            label1.Text = Application.Session["UserID"];
        }
    }
}
