﻿
namespace SubProjectA
{
    partial class Window2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.reportViewer = new SubProjectA.AspNetReportViewer();
            this.SuspendLayout();
            // 
            // aspNetPanel1
            // 
            this.reportViewer.Location = new System.Drawing.Point(4, 4);
            this.reportViewer.Name = "aspNetPanel1";
            this.reportViewer.Size = new System.Drawing.Size(684, 446);
            this.reportViewer.TabIndex = 0;
            this.reportViewer.Text = "aspNetPanel1";
            // 
            // Window2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 453);
            this.Controls.Add(this.reportViewer);
            this.Name = "Window2";
            this.Text = "Window2";
            this.ResumeLayout(false);

        }

        #endregion

        private AspNetReportViewer reportViewer;
    }
}