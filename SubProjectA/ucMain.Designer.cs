﻿
namespace SubProjectA
{
    partial class ucMain
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuBar1 = new Wisej.Web.MenuBar();
            this.menuItem1 = new Wisej.Web.MenuItem();
            this.menuItem2 = new Wisej.Web.MenuItem();
            this.menuItem3 = new Wisej.Web.MenuItem();
            this.menuItem4 = new Wisej.Web.MenuItem();
            this.SuspendLayout();
            // 
            // menuBar1
            // 
            this.menuBar1.BackColor = System.Drawing.Color.DodgerBlue;
            this.menuBar1.Dock = Wisej.Web.DockStyle.Top;
            this.menuBar1.Location = new System.Drawing.Point(0, 0);
            this.menuBar1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.menuItem1,
            this.menuItem2});
            this.menuBar1.Name = "menuBar1";
            this.menuBar1.Size = new System.Drawing.Size(1001, 28);
            this.menuBar1.TabIndex = 0;
            this.menuBar1.TabStop = false;
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.Name = "menuItem1";
            this.menuItem1.Text = "APP A";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 1;
            this.menuItem2.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.menuItem3,
            this.menuItem4});
            this.menuItem2.Name = "menuItem2";
            this.menuItem2.Text = "Window";
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 0;
            this.menuItem3.Name = "menuItem3";
            this.menuItem3.Text = "Show Window";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 1;
            this.menuItem4.Name = "menuItem4";
            this.menuItem4.Text = "Show Report";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // ucMain
            // 
            this.Controls.Add(this.menuBar1);
            this.Name = "ucMain";
            this.Size = new System.Drawing.Size(1001, 700);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Wisej.Web.MenuBar menuBar1;
        private Wisej.Web.MenuItem menuItem1;
        private Wisej.Web.MenuItem menuItem2;
        private Wisej.Web.MenuItem menuItem3;
        private Wisej.Web.MenuItem menuItem4;
    }
}
