﻿using System;
using Wisej.Web;

namespace SubProjectA
{
    public partial class ucMain : Wisej.Web.UserControl
    {
        public ucMain()
        {
            InitializeComponent();
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {

        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            Window1 win1 = new Window1();
            win1.Show();
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
            Window2 win2 = new Window2();
            win2.Show();
        }
    }
}
