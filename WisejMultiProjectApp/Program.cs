﻿using System;
using Wisej.Web;

namespace WisejMultiProjectApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //Window1 window = new Window1();
            //window.Show();
            Page rootPage = new mainPage();
            rootPage.Show();
        }

        //
        // You can use the entry method below
        // to receive the parameters from the URL in the args collection.
        //
        //static void Main(NameValueCollection args)
        //{
        //}
    }
}