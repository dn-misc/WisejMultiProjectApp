﻿using System;
using System.IO;
using Wisej.Web;


namespace WisejMultiProjectApp
{
    public partial class mainPage : Page
    {
        public mainPage()
        {
            InitializeComponent();
        }

        private void navigationBarItem1_Click(object sender, EventArgs e)
        {
            System.Reflection.Assembly asm = System.Reflection.Assembly.Load(File.ReadAllBytes(Application.MapPath("\\bin\\SubProjectA.dll")));
            Type type = asm.GetType("SubProjectA.ucMain");
            Wisej.Web.UserControl mainUC = (Wisej.Web.UserControl)Activator.CreateInstance(type);

            var container = this.mainPanel;
            container.Controls.Clear(true);
            mainUC.Dock = DockStyle.Fill;
            mainUC.Parent = container;
        }

        private void navigationBarItem2_Click(object sender, EventArgs e)
        {
            System.Reflection.Assembly asm = System.Reflection.Assembly.Load(File.ReadAllBytes(Application.MapPath("\\bin\\SubProjectB.dll")));
            Type type = asm.GetType("SubProjectB.ucMain");
            Wisej.Web.UserControl mainUC = (Wisej.Web.UserControl)Activator.CreateInstance(type);

            var container = this.mainPanel;
            container.Controls.Clear(true);
            mainUC.Dock = DockStyle.Fill;
            mainUC.Parent = container;
        }

        private void mainPage_Load(object sender, EventArgs e)
        {
            Application.Session["UserID"] = "DiNo";
        }
    }
}
