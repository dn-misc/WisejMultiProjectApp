﻿
namespace WisejMultiProjectApp
{
    partial class mainPage
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainNavigationBar = new Wisej.Web.Ext.NavigationBar.NavigationBar();
            this.navigationBarItem1 = new Wisej.Web.Ext.NavigationBar.NavigationBarItem();
            this.navigationBarItem2 = new Wisej.Web.Ext.NavigationBar.NavigationBarItem();
            this.mainPanel = new Wisej.Web.Panel();
            this.SuspendLayout();
            // 
            // mainNavigationBar
            // 
            this.mainNavigationBar.CompactView = true;
            this.mainNavigationBar.Dock = Wisej.Web.DockStyle.Left;
            this.mainNavigationBar.Items.AddRange(new Wisej.Web.Ext.NavigationBar.NavigationBarItem[] {
            this.navigationBarItem1,
            this.navigationBarItem2});
            this.mainNavigationBar.Logo = "icon-copy";
            this.mainNavigationBar.Name = "mainNavigationBar";
            this.mainNavigationBar.Size = new System.Drawing.Size(70, 988);
            this.mainNavigationBar.UserAvatar = "icon-question";
            this.mainNavigationBar.UserName = "SomeUser";
            this.mainNavigationBar.UserStatus = "Logged in";
            // 
            // navigationBarItem1
            // 
            this.navigationBarItem1.BackColor = System.Drawing.Color.DodgerBlue;
            this.navigationBarItem1.Icon = "window-icon";
            this.navigationBarItem1.Name = "navigationBarItem1";
            this.navigationBarItem1.Text = "App1";
            this.navigationBarItem1.Click += new System.EventHandler(this.navigationBarItem1_Click);
            // 
            // navigationBarItem2
            // 
            this.navigationBarItem2.BackColor = System.Drawing.Color.OrangeRed;
            this.navigationBarItem2.Icon = "window-icon";
            this.navigationBarItem2.Name = "navigationBarItem2";
            this.navigationBarItem2.Text = "App2";
            this.navigationBarItem2.Click += new System.EventHandler(this.navigationBarItem2_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.mainPanel.Location = new System.Drawing.Point(76, 3);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(1817, 982);
            this.mainPanel.TabIndex = 1;
            this.mainPanel.TabStop = true;
            // 
            // mainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.mainNavigationBar);
            this.Name = "mainPage";
            this.Size = new System.Drawing.Size(1896, 988);
            this.Load += new System.EventHandler(this.mainPage_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Wisej.Web.Ext.NavigationBar.NavigationBar mainNavigationBar;
        private Wisej.Web.Panel mainPanel;
        private Wisej.Web.Ext.NavigationBar.NavigationBarItem navigationBarItem1;
        private Wisej.Web.Ext.NavigationBar.NavigationBarItem navigationBarItem2;
    }
}
